#include "application.hpp"

Document* Application::open_document(const std::string& doc_name)
{
	Document* ptr_doc = create_document();
	ptr_doc->open(doc_name);
	documents_.push_back(ptr_doc);

	return ptr_doc;
}

Document* Application::find_document(const std::string& title)
{
	std::list<Document*>::iterator it = documents_.begin();
	std::list<Document*>::iterator end = documents_.end();
	for( ; it != end; ++it)
		if ((*it)->get_title() == title)
			return *it;

	return NULL;
}

Application::Application()
{
}

Application::~Application()
{
	std::list<Document*>::iterator it = documents_.begin();
	std::list<Document*>::iterator end = documents_.end();
	for( ; it != end; ++it)
		delete *it;
}




