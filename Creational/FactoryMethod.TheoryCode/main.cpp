#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>

#include "factory.hpp"

using namespace std;

class Client
{
    shared_ptr<ServiceCreator> creator_;
public:
    Client(shared_ptr<ServiceCreator> creator) : creator_(creator)
	{
	}

    Client(const Client&) = delete;
    Client& operator=(const Client&) = delete;

    void use()
	{
        unique_ptr<Service> service = creator_->create_service();

        string result = service->run();
        cout << "Client is using: " << result << endl;
	}
};

int main()
{
    vector<int> collection;

//    for(const auto& item : collection)
//    {
//        cout << item << " ";
//    }

    for(auto it = collection.begin(); it != collection.end(); ++it)
    {
        const auto& item = *it;

        cout << item << " ";
    }


    typedef std::map<std::string, shared_ptr<ServiceCreator>> Factory;
    Factory creators;
    creators.insert(make_pair("CreatorA", make_shared<ConcreteCreatorA>()));
    creators.insert(make_pair("CreatorB", make_shared<ConcreteCreatorB>()));
    creators.insert(make_pair("CreatorC", make_shared<ConcreteCreatorC>()));

    Client client(creators["CreatorC"]);
	client.use();
}
