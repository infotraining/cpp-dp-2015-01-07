#include <iostream>
#include <string>
#include <fstream>
#include <cassert>
#include <map>
#include <algorithm>
#include <stdexcept>

#include "shape.hpp"
#include "clone_factory.hpp"
#include "application.hpp"

#include <boost/bind.hpp>

using namespace Drawing;
using namespace std;

class GraphicsDocument : public Document
{
	vector<Shape*> shapes_;
	string title_;
public:
	virtual void open(const string& file_name)
	{
		title_ = file_name;

		ifstream fin(file_name.c_str());

		string type_identifier;

		cout << "Loading a file...\n";
		while(!fin.eof())
		{
			if (fin >> type_identifier)
			{
				cout << type_identifier << "\n";

				Shape* shp_ptr = ShapeFactory::instance().create(type_identifier);
				shp_ptr->read(fin);

				shapes_.push_back(shp_ptr);
			}
		}

		cout << "Loading finished." << endl;
	}

	virtual void save(const std::string& file_name)
	{
		ofstream fout(file_name.c_str());
		for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::write, _1, boost::ref(fout)));
		fout.close();

		title_ = file_name;
	}

	virtual string get_title() const
	{
		return title_;
	}

	void show()
	{
		cout << "\n\nDrawing:\n";
		for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::draw, _1));
	}

    ~GraphicsDocument()
    {
        for(std::vector<Shape*>::iterator it = shapes_.begin(); it != shapes_.end(); ++it)
            delete *it;
    }
};

class GraphicsApplication : public Application
{
public:
	virtual Document* create_document()
	{
		return new GraphicsDocument();
	}
};

int main()
{
	GraphicsApplication app;

	app.open_document("drawing.txt");

	Document* ptr_doc = app.find_document("drawing.txt");

	ptr_doc->show();

	ptr_doc->save("new_drawing.txt");
}
