#ifndef ENEMY_FACTORY_HPP_
#define ENEMY_FACTORY_HPP_

#include <memory>
#include "monsters.hpp"

namespace Game
{

class AbstractEnemyFactory
{
public:
    virtual std::unique_ptr<Enemy> CreateSoldier() = 0;
    virtual std::unique_ptr<Enemy> CreateMonster() = 0;
    virtual std::unique_ptr<Enemy> CreateSuperMonster() = 0;
    virtual ~AbstractEnemyFactory() = default;
};

class EasyLevelEnemyFactory : public AbstractEnemyFactory
{
public:
    std::unique_ptr<Enemy> CreateSoldier() override
	{
        return std::unique_ptr<Enemy>(new SillySoldier());
	}
	
    std::unique_ptr<Enemy> CreateMonster() override
	{
        return std::unique_ptr<Enemy>(new SillyMonster());
	}
	
    std::unique_ptr<Enemy> CreateSuperMonster() override
	{
        return std::unique_ptr<Enemy>(new SillySuperMonster());
	}
};

class DieHardLevelEnemyFactory : public AbstractEnemyFactory
{
public:
    std::unique_ptr<Enemy> CreateSoldier() override
    {
        return std::unique_ptr<Enemy>(new BadSoldier());
    }

    std::unique_ptr<Enemy> CreateMonster() override
    {
        return std::unique_ptr<Enemy>(new BadMonster());
    }

    std::unique_ptr<Enemy> CreateSuperMonster() override
    {
        return std::unique_ptr<Enemy>(new BadSuperMonster());
    }
};


}


#endif /*ENEMY_FACTORY_HPP_*/
