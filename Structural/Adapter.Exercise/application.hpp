#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include <list>
#include <string>

class Document
{
public:
	virtual std::string get_title() const = 0;
	virtual void open(const std::string& file_name) = 0;
	virtual void save(const std::string& file_name) = 0;
	virtual void show() = 0;
	virtual ~Document() {};
};

class Application
{
public:
	Application();
	Document* open_document(const std::string& doc_name);
	Document* find_document(const std::string& title);
	virtual ~Application();
protected:
	virtual Document* create_document() = 0;
private:
	std::list<Document*> documents_;

	Application(const Application&);
	Application& operator=(const Application&);
};


#endif
