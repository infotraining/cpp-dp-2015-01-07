#ifndef BITMAP_HPP
#define BITMAP_HPP

#include <vector>
#include <memory>
#include <iostream>
#include <boost/scoped_ptr.hpp>

class Bitmap
{  
    class Impl;

    std::shared_ptr<Impl> impl_;
public:
    Bitmap(std::size_t size);

    void draw() const;

    //~Bitmap();
};

#endif // BITMAP_HPP


