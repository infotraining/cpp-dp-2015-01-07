#include "bitmap.hpp"
#include <algorithm>
#include <iostream>
#include <vector>
#include <deque>
#include <boost/ref.hpp>

struct Bitmap::Impl
{
    std::vector<char> buffer_;

    Impl(size_t size, char c) : buffer_(size, c)
    {
            std::cout << "Impl()" << std::endl;
    }

    ~Impl()
    {
        std::cout << "~Impl()" << std::endl;
    }
};

//Bitmap::~Bitmap() = default;

Bitmap::Bitmap(size_t size)
{
    impl_.reset(new Impl(size, 65));
}

void Bitmap::draw() const
{
    for(size_t i = 0; i < impl_->buffer_.size(); ++i)
        std::cout << impl_->buffer_[i];
    std::cout << std::endl;
}
